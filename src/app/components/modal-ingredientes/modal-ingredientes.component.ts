import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Router } from '@angular/router';

@Component({
  selector: 'app-modal-ingredientes',
  templateUrl: './modal-ingredientes.component.html',
  styleUrls: ['./modal-ingredientes.component.css']
})
export class ModalIngredientesComponent implements OnInit {

  constructor( public dialogRef: MatDialogRef<ModalIngredientesComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any, private router: Router) { }

  ngOnInit(): void {

  }

  onNoClick(): void{
    this.dialogRef.close();
     }

     detalle(id:string){
   this.router.navigate(["dashboard/platillos" ,id ]);
   this.dialogRef.close();
     }


}
