import { AuthService } from './../../services/auth.service';
import { ModalIngredientesComponent } from './../modal-ingredientes/modal-ingredientes.component';
import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ApiServicesService } from 'src/app/services/api-services.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {

  ingrediente: any = null;
  usuarioLogeado: boolean = false;

  constructor(private router: Router,
    private sv: ApiServicesService,
    private auth: AuthService,
    public dialog: MatDialog,) { }

  ngOnInit(): void {
    this.sv.getIngredientes().subscribe(
      res => {
        this.ingrediente = res;
      }
    )
    this.usuarioLogeado =  this.auth.isLoggedIn('');
    this.auth.changeLoginStatus$.subscribe(
      (loggedStatus: boolean) => {
        this.usuarioLogeado = loggedStatus
      }
    )
  }

  redirect(view:string){
    if(view == 'ingredientes'){
      this.openDialog();
    }  else {
      this.router.navigate([`/dashboard/${view}`])
    }


  }

  openDialog(){
    const dialogRef = this.dialog.open(ModalIngredientesComponent, {
      width: '350px',
      data:  this.ingrediente
    });

    dialogRef.afterClosed().subscribe( res => {
    })
  }

  logout(){
    this.auth.logout();
    this.router.navigate(['/login']);
  }
}
