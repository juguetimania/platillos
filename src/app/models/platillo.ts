export interface Platillo {
  idMeal: string,
  strMeal: string,
  strMealThumb: string
}
