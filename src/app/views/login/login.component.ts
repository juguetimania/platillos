import { AuthService } from './../../services/auth.service';
import { Keys } from './../../utils/keys';
import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  logo: string = 'assets/img/logo.png';
  invalido: boolean = true;
  form = this.fb.group({
    usuario: [''],
    password: ['']
  })

  routerRedirect: string = '';

  constructor(private router: Router, private fb: FormBuilder, private auth: AuthService) {
   }

  ngOnInit(): void {
  }

  submit() {
    const {usuario , password } = this.form.value;
    if(usuario == Keys.USUARIO && password == Keys.PASSWORD){
      this.auth.login();
      this.routerRedirect = this.auth.urlUsuarioAcceder;
      this.auth.urlUsuarioAcceder = '';
      this.router.navigateByUrl('/dashboard');
    } else {
      this.invalido = false;
      this.form.reset();
    }

  }

  reset(){
    this.invalido = true;
  }

}
