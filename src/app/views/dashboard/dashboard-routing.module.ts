import { CanActivateGuard } from './../../can-activate.guard';
import { PerfilComponent } from './../perfil/perfil.component';
import { PlatillosComponent } from './../platillos/platillos.component';
import { DetalleComponent } from './../detalle/detalle.component';
import { DashboardComponent } from './dashboard.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from '../home/home.component';

const routes: Routes = [
  {path: '', component: DashboardComponent, canActivate: [CanActivateGuard], children: [
  {path: '', component: HomeComponent},
  {path: 'platillos/:id', component: PlatillosComponent},
  {path: 'platillos', component: PlatillosComponent},
  {path: 'usuario', component: PerfilComponent},
  {path: 'detalle/:id', component: DetalleComponent},
  ]},

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DashboardRoutingModule { }
