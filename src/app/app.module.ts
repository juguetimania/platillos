import { HomeComponent } from './views/home/home.component';


import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { SharedModule } from './components/shared/shared/shared.module';
import { LoginComponent } from './views/login/login.component';
import { ReactiveFormsModule } from '@angular/forms';
import { DashboardComponent } from './views/dashboard/dashboard.component';
import { HttpClientModule } from '@angular/common/http';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ModalComponent } from './components/modal/modal.component';
import { DetalleComponent } from './views/detalle/detalle.component';
import { ModalDetalleComponent } from './components/modal-detalle/modal-detalle.component';
import { MatCarouselModule } from '@ngmodule/material-carousel';
import { PlatillosComponent } from './views/platillos/platillos.component';
import { ModalIngredientesComponent } from './components/modal-ingredientes/modal-ingredientes.component';
import { NofoundComponent } from './views/nofound/nofound.component';
import { PerfilComponent } from './views/perfil/perfil.component';
@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    DashboardComponent,
    ModalComponent,
    DetalleComponent,
    ModalDetalleComponent,
    PlatillosComponent,
    ModalIngredientesComponent,
    NofoundComponent,
    PerfilComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    HttpClientModule,
    SharedModule,
    MatCarouselModule.forRoot()
  ],
  entryComponents: [HomeComponent],
  providers: [   { provide: MAT_DIALOG_DATA, useValue: {} }, { provide: MatDialogRef, useValue: {} }],
  bootstrap: [AppComponent]
})
export class AppModule { }
