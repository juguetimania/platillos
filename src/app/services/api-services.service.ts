import { environment } from './../../environments/environment';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ApiServicesService {

  constructor(private http: HttpClient) { }


  getPlatillos(): Observable<any> {
    return this.http.get(`${environment.urlBase}filter.php?c=Seafood`)
    .pipe(map( (res:any) => res['meals']));

  }

  getRandom():Observable<any> {
    return this.http.get(`${environment.urlBase}random.php`)
    .pipe(map( (res:any) => res['meals']));
  }

  getBuscarId(id:string):Observable<any> {
    return this.http.get(`${environment.urlBase}lookup.php?i=${id}`)
    .pipe(map( (res:any) => res['meals']));
  }

  getIngredientes():Observable<any> {
    return this.http.get(`${environment.urlBase}list.php?i=list`)
    .pipe(map( (res:any) => res['meals']));
  }

  getIngredientePlatilllos(id:string):Observable<any> {
    return this.http.get(`${environment.urlBase}search.php?s=${id}`)
    .pipe(map( (res:any) => res['meals']));
  }

}
